#ifndef _GLOBAL_VARS_
#define _GLOBAL_VARS_
	
	
    #include    <map>
	#include 	<sstream>

	#define 	BTN_1 7
	#define 	BTN_2 0
	#define 	BTN_3 21
	#define 	BTN_4 22
	#define 	BACKLIGHT 30

	#define		ON HIGH
	#define		OFF !ON

	#define		KEYWORD_SIZE	4
    
#endif