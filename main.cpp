#include    <wiringPi.h>
#include    <iostream>
#include    <string>

#include    "global_variables.h"
#include    "global_functions.cpp"
#include    "PCD8544/PCD8544.cpp"
#include    "PCD8544/Fonts/big_number_digital.cpp"
#include    "PCD8544/Fonts/font5x5.c"
#define _GLCD_DATA_BYTEY_


int main(int argc, char const *argv[]) {

    wiringPiSetup();
    LCD *LCD_inner  =   new LCD();
    LCD *LCD_outer  =   new LCD(1, 27, 24, 23, 26);

    pin_init();

    std::string foo = "TA THUONG TOI BUA QUEN AN NUA DEM VO GOI RUOT DAU NHU CAT NUOC MAT DAM DIA CHI CAM TUC KHONG DUOC LOT DA";
    LCD_outer->LCDclear();
    LCD_outer->LCDsetContrast(50);
    LCD_outer->LCDdrawstring_P(0, 0, foo.c_str());
    LCD_outer->LCDdisplay();

    foo = "TA THUONG TOI BUA QUEN AN NUA DEM VO GOI RUOT DAU NHU CAT NUOC MAT DAM DIA CHI CAM TUC KHONG DUOC LOT DA";
    LCD_inner->LCDclear();
    LCD_inner->LCDsetFont(font5x5);
    LCD_inner->LCDdrawstring_P(0, 0, foo.c_str());
    LCD_inner->LCDdisplay();
    return 0;
}