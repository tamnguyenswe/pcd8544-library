/*
    =================================================================================
    This library is based on Limor Fried's library: https://github.com/binerry/RaspberryPi/tree/master/libraries/c/PCD8544
    with some extended functions
    ================================================================================
 */
#include <wiringPi.h>
#include "./definitions.cpp"
#include "./Fonts/font5x7.cpp"
#include "./Fonts/pi_logo.cpp"

// // reduces how much is refreshed, which speeds it up!
// // originally derived from Steve Evans/JCW's mod but cleaned up and optimized
// //#define enablePartialUpdate

// #ifdef enablePartialUpdate
//     static uint8_t xUpdateMin, xUpdateMax, yUpdateMin, yUpdateMax;
// #endif

// static void updateBoundingBox(uint8_t xmin, uint8_t ymin, uint8_t xmax, uint8_t ymax) {
    // #ifdef enablePartialUpdate
    //     if (xmin < xUpdateMin) xUpdateMin = xmin;
    //     if (xmax > xUpdateMax) xUpdateMax = xmax;
    //     if (ymin < yUpdateMin) yUpdateMin = ymin;
    //     if (ymax > yUpdateMax) yUpdateMax = ymax;
    // #endif
// }

#ifndef _LCD_
#define _LCD_

struct Font 
{
    const unsigned char *currentFont;
    unsigned int width;
    unsigned int height;
    unsigned int firstCharIndex;
    unsigned int numberOfChars;
};


class LCD 
{
    public:
        //default LCD onboard
        LCD() 
        {
            LCDInit(DEFAULT_SCLK, DEFAULT_DIN, DEFAULT_DC, DEFAULT_CS, DEFAULT_RST, DEFAULT_CONTRAST);
        }

        //any extended LCDs
        LCD(uint8_t SCLK, uint8_t DIN, uint8_t DC, uint8_t CS, uint8_t RST) 
        {
            LCDInit(SCLK, DIN, DC, CS, RST, DEFAULT_CONTRAST);
        }

        //Font width MUST be FIXED! Dynamic font width NOT SUPPORTED!
        void LCDsetFont(const unsigned char fontToSet[]) 
        {
            this->font.currentFont      =   fontToSet;

            this->font.width            =   fontToSet[0];
            this->font.height           =   fontToSet[1];
            this->font.firstCharIndex   =   fontToSet[2];
            this->font.numberOfChars    =   fontToSet[3];

            std::cout << "font width: " << this->font.width << std::endl;
            std::cout << "font height: " << this->font.height << std::endl;
            std::cout << "first char index: " << this->font.firstCharIndex << std::endl;
            std::cout << "number of chars: " << this->font.numberOfChars << std::endl;
        }

        void LCDsetPixel(uint8_t x, uint8_t y, uint8_t color)
        {
            if ((x >= LCDWIDTH) || (y >= LCDHEIGHT))
                return;
            // x is which column
            if (color)
                this->pcd8544_buffer[x+ (y/8)*LCDWIDTH] |= _BV(y%8);
                // this->pcd8544_buffer[x+ (y/8)*LCDWIDTH] |= _BV(y%(this->font.height + 1));
            else
                // this->pcd8544_buffer[x+ (y/8)*LCDWIDTH] |= ~_BV(y%8);
                this->pcd8544_buffer[x+ (y/8)*LCDWIDTH] &= ~_BV(y%8);
            delay(MAGIC_DEBUG_VALUE);
        }

        void LCDshowLogo()
        {
            uint32_t i;
            for (i = 0; i < LCDWIDTH * LCDHEIGHT / 8; i++)
            {
                this->pcd8544_buffer[i] = pi_logo[i];
            }
            LCDdisplay();
        }

        void LCDdrawbitmap(uint8_t x, uint8_t y,const uint8_t *bitmap, uint8_t w, uint8_t h, uint8_t color)
        {
            uint8_t j,i;
            for (j = 0; j < h; j++)
            {
                for (i = 0; i < w; i++ )
                {
                    if (*(bitmap + i + (j / 8) * w) & _BV(j % 8))
                    {
                        LCDsetPixel(x + i, y + j, color);
                    }
                }
            }
            // updateBoundingBox(x, y, x + w, y + h);
        }

        void LCDdrawstring(uint8_t x, uint8_t y, char *c)
        {
            this->cursor_x = x;
            this->cursor_y = y;
            while (*c)
            {
                LCDwrite(*c++);
            }
        }

        void LCDdrawstring_P(uint8_t x, uint8_t y, const char *str)
        {
            this->cursor_x = x;
            this->cursor_y = y;
            while (1)
            {
                char c = (*str++);
                if (! c)
                    return;
                LCDwrite(c);
            }
        }

        void LCDdrawchar(uint8_t x, uint8_t y, char c)
        {
            if ((y >= LCDHEIGHT) ||
               ((x + this->font.width) >= LCDWIDTH)) 
            {
                return;
            }

            uint8_t i,j;
            for (i = 0; i < this->font.width; i++)
            {
                uint8_t d = *(this->font.currentFont + ((c - this->font.firstCharIndex) * this->font.width) + i + FONT_INFO_RESERVED);
                uint8_t j;
                for (j = 0; j < this->font.height + 1; j++)
                {
                    if (d & _BV(j))
                    {
                        LCDsetPixel(x + i, y + j, this->textcolor);
                    }
                    else
                    {
                        LCDsetPixel(x + i, y + j, !this->textcolor);
                    }
                }
            }

            //white line at new char's rightside
            for (j = 0; j < this->font.height + 1; j++)
            {
                LCDsetPixel(x + this->font.width, y + j, !this->textcolor);
            }
            // updateBoundingBox(x, y, x + this->font.width, y + this->font.height + 1);
        }

        void LCDwrite(uint8_t c)
        {
            if (c == '\n')
            {
                this->cursor_y += this->font.height + 1;
                this->cursor_x = 0;
            }
            else if (c == '\r')
            {
                // skip em
            }
            else
            {
                LCDdrawchar(this->cursor_x, this->cursor_y, c);
                this->cursor_x += this->font.width + 1;

                if (this->cursor_x >= (LCDWIDTH - this->font.width))
                {
                    this->cursor_x = 0;
                    this->cursor_y += this->font.height + 1;
                }
                if (this->cursor_y >= LCDHEIGHT)
                    this->cursor_y = 0;
            }
        }

        void LCDsetCursor(uint8_t x, uint8_t y)
        {
            this->cursor_x = x;
            this->cursor_y = y;
        }

        // bresenham's algorithm - thx wikpedia
        void LCDdrawline(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, uint8_t color)
        {
            uint8_t steep = abs(y1 - y0) > abs(x1 - x0);
            if (steep)
            {
                swap(x0, y0);
                swap(x1, y1);
            }

            if (x0 > x1)
            {
                swap(x0, x1);
                swap(y0, y1);
            }

            // much faster to put the test here, since we've already sorted the points
            // updateBoundingBox(x0, y0, x1, y1);

            uint8_t dx, dy;
            dx = x1 - x0;
            dy = abs(y1 - y0);

            int8_t err = dx / 2;
            int8_t ystep;

            if (y0 < y1)
            {
                ystep = 1;
            } else
            {
                ystep = -1;
            }

            for (; x0<=x1; x0++)
            {
                if (steep)
                {
                    LCDsetPixel(y0, x0, color);
                }
                else
                {
                    LCDsetPixel(x0, y0, color);
                }
                err -= dy;
                if (err < 0)
                {
                    y0 += ystep;
                    err += dx;
                }
            }
        }

        // filled rectangle
        void LCDfillrect(uint8_t x, uint8_t y, uint8_t w, uint8_t h,  uint8_t color)
        {
            // stupidest version - just pixels - but fast with internal buffer!
            uint8_t i,j;
            for (i = x; i < x + w; i++)
            {
                for (j = y;j < y + h; j++)
                {
                    LCDsetPixel(i, j, color);
                }
            }
            // updateBoundingBox(x, y, x + w, y + h);
        }

        // draw a rectangle
        void LCDdrawrect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t color)
        {
            // stupidest version - just pixels - but fast with internal buffer!
            uint8_t i;
            for (i = x; i < x + w; i++) {
                LCDsetPixel(i, y, color);
                LCDsetPixel(i, y + h - 1, color);
            }
            for (i = y; i < y + h; i++) {
                LCDsetPixel(x, i, color);
                LCDsetPixel(x + w - 1, i, color);
            }

            // updateBoundingBox(x, y, x + w, y + h);
        }

        // draw a circle outline
        void LCDdrawcircle(uint8_t x0, uint8_t y0, uint8_t r, uint8_t color)
        {
            // updateBoundingBox(x0 - r, y0 - r, x0 + r, y0 + r);

            int8_t f = 1 - r;
            int8_t ddF_x = 1;
            int8_t ddF_y = -2 * r;
            int8_t x = 0;
            int8_t y = r;

            LCDsetPixel(x0, y0 + r, color);
            LCDsetPixel(x0, y0 - r, color);
            LCDsetPixel(x0 + r, y0, color);
            LCDsetPixel(x0 - r, y0, color);

            while (x < y)
            {
                if (f >= 0)
                {
                    y--;
                    ddF_y += 2;
                    f += ddF_y;
                }
                x++;
                ddF_x += 2;
                f += ddF_x;

                LCDsetPixel(x0 + x, y0 + y, color);
                LCDsetPixel(x0 - x, y0 + y, color);
                LCDsetPixel(x0 + x, y0 - y, color);
                LCDsetPixel(x0 - x, y0 - y, color);

                LCDsetPixel(x0 + y, y0 + x, color);
                LCDsetPixel(x0 - y, y0 + x, color);
                LCDsetPixel(x0 + y, y0 - x, color);
                LCDsetPixel(x0 - y, y0 - x, color);

            }
        }

        void LCDfillcircle(uint8_t x0, uint8_t y0, uint8_t r, uint8_t color)
        {
            // updateBoundingBox(x0 - r, y0 - r, x0 + r, y0 + r);
            int8_t f = 1 - r;
            int8_t ddF_x = 1;
            int8_t ddF_y = -2 * r;
            int8_t x = 0;
            int8_t y = r;
            uint8_t i;

            for (i = y0 - r; i <= y0 + r; i++)
            {
                LCDsetPixel(x0, i, color);
            }

            while (x < y)
            {
                if (f >= 0)
                {
                    y--;
                    ddF_y += 2;
                    f += ddF_y;
                }
                x++;
                ddF_x += 2;
                f += ddF_x;

                for (i = y0 - y; i <= y0 + y; i++)
                {
                    LCDsetPixel(x0+x, i, color);
                    LCDsetPixel(x0-x, i, color);
                }
                for (i = y0 - x; i <= y0 + x; i++)
                {
                    LCDsetPixel(x0+y, i, color);
                    LCDsetPixel(x0-y, i, color);
                }
            }
        }

        // the most basic function, set a single pixel
        // void LCDsetPixel(uint8_t x, uint8_t y, uint8_t color)
        // {
        //     if ((x >= LCDWIDTH) || (y >= LCDHEIGHT))
        //         return;

        //     // x is which column
        //     if (color)
        //         this->pcd8544_buffer[x+ (y/8)*LCDWIDTH] |= _BV(y%8);
        //     else
        //         this->pcd8544_buffer[x+ (y/8)*LCDWIDTH] &= ~_BV(y%8);
        //     updateBoundingBox(x,y,x,y);
        // }

        // the most basic function, get a single pixel
        uint8_t LCDgetPixel(uint8_t x, uint8_t y)
        {
            if ((x >= LCDWIDTH) || (y >= LCDHEIGHT))
                return 0;

            return (this->pcd8544_buffer[x + (y/8)*LCDWIDTH] >> ( 7 - ( y % 8))) & 0x1;
        }

        void LCDsetContrast(uint8_t val)
        {
            if (val > 0x7f) {
                val = 0x7f;
            }
            LCDcommand(PCD8544_FUNCTIONSET | PCD8544_EXTENDEDINSTRUCTION );
            LCDcommand( PCD8544_SETVOP | val);
            LCDcommand(PCD8544_FUNCTIONSET);
        }

        void LCDdisplay(void)
        {
            uint8_t col, maxcol, p;

            for(p = 0; p < 6; p++)
            {

            // #ifdef enablePartialUpdate
            //     // check if this page is part of update
            //     if ( yUpdateMin >= ((p+1)*8) )
            //     {
            //         continue;   // nope, skip it!
            //     }
            //     if (yUpdateMax < p*8)
            //     {
            //         break;
            //     }
            // #endif

                LCDcommand(PCD8544_SETYADDR | p);


            // #ifdef enablePartialUpdate
            //     col = xUpdateMin;
            //     maxcol = xUpdateMax;
            // #else
            //     // start at the beginning of the row
                col = 0;
                maxcol = LCDWIDTH-1;
            // #endif

                LCDcommand(PCD8544_SETXADDR | col);

                for(; col <= maxcol; col++) {
                    //uart_putw_dec(col);
                    //uart_putchar(' ');
                    LCDdata(this->pcd8544_buffer[(LCDWIDTH*p)+col]);
                }
            }

            LCDcommand(PCD8544_SETYADDR);  // no idea why this is necessary but it is to finish the last byte?
            // #ifdef enablePartialUpdate
            // xUpdateMin = LCDWIDTH - 1;
            // xUpdateMax = 0;
            // yUpdateMin = LCDHEIGHT-1;
            // yUpdateMax = 0;
            // #endif

        }

        // clear everything
        void LCDclear(void) 
        {
            //memset(this->pcd8544_buffer, 0, LCDWIDTH*LCDHEIGHT/8);
            uint32_t i;
            for ( i = 0; i < LCDWIDTH*LCDHEIGHT/8 ; i++)
                this->pcd8544_buffer[i] = 0;
            // updateBoundingBox(0, 0, LCDWIDTH-1, LCDHEIGHT-1);
            this->cursor_y = this->cursor_x = 0;
        }

    protected:
        // the memory buffer for the LCD    
        uint8_t pcd8544_buffer[LCDWIDTH * LCDHEIGHT / 8] = {0,};
        
        uint8_t cursor_x, cursor_y, textsize, textcolor;
        int8_t _din, _sclk, _dc, _rst, _cs;

        struct Font font;

        unsigned int line_spacing_value;


        void LCDInit(uint8_t SCLK, uint8_t DIN, uint8_t DC, uint8_t CS, uint8_t RST, uint8_t contrast) 
        {
            this->_din = DIN;
            this->_sclk = SCLK;
            this->_dc = DC;
            this->_rst = RST;
            this->_cs = CS;
            this->cursor_x = this->cursor_y = 0;
            this->textsize = 1;
            this->textcolor = BLACK;

            // set pin directions
            pinMode(this->_din, OUTPUT);
            pinMode(this->_sclk, OUTPUT);
            pinMode(this->_dc, OUTPUT);
            pinMode(this->_rst, OUTPUT);
            pinMode(this->_cs, OUTPUT);

            // toggle RST low to reset; CS low so it'll listen to us
            if (this->_cs > 0)
                digitalWrite(this->_cs, LOW);

            digitalWrite(this->_rst, LOW);
            _delay_ms(500);
            digitalWrite(this->_rst, HIGH);

            // get into the EXTENDED mode!
            LCDcommand(PCD8544_FUNCTIONSET | PCD8544_EXTENDEDINSTRUCTION );

            // LCD bias select (4 is optimal?)
            LCDcommand(PCD8544_SETBIAS | 0x4);

            // set VOP
            if (contrast > 0x7f)
                contrast = 0x7f;

            LCDcommand( PCD8544_SETVOP | contrast); // Experimentally determined

            // normal mode
            LCDcommand(PCD8544_FUNCTIONSET);

            // Set display to Normal
            LCDcommand(PCD8544_DISPLAYCONTROL | PCD8544_DISPLAYNORMAL);

            // set up a bounding box for screen updates
            // updateBoundingBox(0, 0, LCDWIDTH-1, LCDHEIGHT-1);

            // set default font
            LCDsetFont(font5x7);
        }

        void LCDspiwrite(uint8_t c)
        {
            shiftOut(this->_din, this->_sclk, MSBFIRST, c);
        }

        void LCDcommand(uint8_t c)
        {
            digitalWrite( this->_dc, LOW);
            LCDspiwrite(c);
        }

        void LCDdata(uint8_t c)
        {
            digitalWrite(this->_dc, HIGH);
            LCDspiwrite(c);
        }        

        // bitbang serial shift out on select GPIO pin. Data rate is defined by CPU clk speed and CLKCONST_2. 
        // Calibrate these value for your need on target platform.
        void shiftOut(uint8_t dataPin, uint8_t clockPin, uint8_t bitOrder, uint8_t val)
        {
            uint8_t i;
            uint32_t j;

            for (i = 0; i < 8; i++)  {
                if (bitOrder == LSBFIRST)
                    digitalWrite(dataPin, !!(val & (1 << i)));
                else
                    digitalWrite(dataPin, !!(val & (1 << (7 - i))));

                digitalWrite(clockPin, HIGH);
                for (j = CLKCONST_2; j > 0; j--); // clock speed, anyone? (LCD Max CLK input: 4MHz)
                digitalWrite(clockPin, LOW);
            }
        }

        // roughly calibrated spin delay
        void _delay_ms(uint32_t t)
        {
            uint32_t nCount = 0;
            while (t != 0)
            {
                nCount = CLKCONST_1;
                while(nCount != 0)
                    nCount--;
                t--;
            }
        }
};

#endif